import model.Monomial;
import model.Polynomial;
import org.junit.jupiter.api.Test;
import model.Operation;

public class MultiplyTest {
    @Test
    public void multiplyTest(){
        Polynomial p1=new Polynomial();
        Polynomial p2=new Polynomial();
        p1.addMonomial(new Monomial(1,5));
        p1.addMonomial(new Monomial(-2,3));
        p1.addMonomial(new Monomial(6,1));
        p2.addMonomial(new Monomial(2,6));
        p2.addMonomial(new Monomial(4,3));
        p2.addMonomial(new Monomial(2,0));
        assert(Operation.multiplication(p1,p2).toString().equals("2x^11-4x^9+4x^8+12x^7-8x^6+2x^5+24x^4-4x^3+12x"));
    }
}

