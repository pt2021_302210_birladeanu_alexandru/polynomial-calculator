import model.Monomial;
import model.Polynomial;
import org.junit.jupiter.api.Test;
import model.Operation;

public class AddTest {
    @Test
    public void addTest(){
        Polynomial p1=new Polynomial();
        Polynomial p2=new Polynomial();
        p1.addMonomial(new Monomial(1,5));
        p1.addMonomial(new Monomial(-2,3));
        p1.addMonomial(new Monomial(6,1));
        p2.addMonomial(new Monomial(2,6));
        p2.addMonomial(new Monomial(4,3));
        p2.addMonomial(new Monomial(2,0));
        assert(Operation.addition(p1,p2).toString().equals("2x^6+x^5+2x^3+6x+2"));
    }
}
