import controller.Controller;
import view.DashboardView;

public class Main {
    public static void main(String[] args) {
        new Controller(new DashboardView());
    }
}
