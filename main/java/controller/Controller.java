package controller;

import model.Monomial;
import model.Operation;
import model.Polynomial;
import model.enums.OperationChoice;
import model.enums.PolynomialChoice;
import view.DashboardView;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {
    private final DashboardView dashboard;

    public Controller(DashboardView newDashboard) {

        dashboard = newDashboard;

        dashboard.comboBoxListener(e -> {
            if (dashboard.getOperationComboBox().equals(OperationChoice.INTEGRAL) || dashboard.getOperationComboBox().equals(OperationChoice.DERIVATIVE)) {
                dashboard.getSelectLabel().setVisible(true);
                dashboard.getPolynomialComboBox().setVisible(true);
            } else {
                dashboard.getSelectLabel().setVisible(false);
                dashboard.getPolynomialComboBox().setVisible(false);
            }
        });

        dashboard.runButtonListener(e -> {
            Polynomial p1 = new Polynomial();
            Polynomial p2 = new Polynomial();

            Pattern pattern = Pattern.compile("([+-]?[^-+]+)");
            Matcher matcher = pattern.matcher(dashboard.getPolynomial1());
            parseInput(p1, matcher);

            matcher = pattern.matcher(dashboard.getPolynomial2());
            parseInput(p2, matcher);

            Polynomial result;
            Polynomial remainder;//for division only
            switch (dashboard.getOperationComboBox()) {
                default -> {
                    result = Operation.addition(p1, p2);
                    dashboard.setResult(result.toString());
                }
                case SUBTRACT -> {
                    result = Operation.subtraction(p1, p2);
                    dashboard.setResult(result.toString());
                }
                case MULTIPLY -> {
                    result = Operation.multiplication(p1, p2);
                    dashboard.setResult(result.toString());
                }
                case DIVIDE -> {
                    result = Operation.division(p1, p2);
                    remainder = Operation.subtraction(p1, Operation.multiplication(p2, result));
                    dashboard.setResult(result.toString() + "; remainder=" + remainder.toString());
                }
                case INTEGRAL -> {
                    if (dashboard.getPolynomialChoice().equals(PolynomialChoice.FIRST))
                        result = Operation.integral(p1);
                    else
                        result = Operation.integral(p2);
                    dashboard.setResult(result.toString());
                }
                case DERIVATIVE -> {
                    if (dashboard.getPolynomialChoice().equals(PolynomialChoice.FIRST))
                        result = Operation.derivative(p1);
                    else
                        result = Operation.derivative(p2);
                    dashboard.setResult(result.toString());
                }
            }
        });

        dashboard.getRunButton().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                dashboard.getRunButton().setForeground(new Color(0, 32, 63));
                dashboard.getRunButton().setBackground(new Color(173, 239, 209));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                dashboard.getRunButton().setBackground(new Color(0, 32, 63));
                dashboard.getRunButton().setForeground(new Color(173, 239, 209));
            }
        });
    }

    private void parseInput(Polynomial p, Matcher matcher) {

        String coef, degree;
        while (matcher.find()) {
            if (matcher.group().contains("x")) {
                coef = matcher.group().substring(0, matcher.group().lastIndexOf('x'));
                if (matcher.group().contains("*"))
                    coef = coef.substring(0, coef.length() - 1);
                if (coef.equals("") || coef.equals("+"))
                    coef = "1";
                if (coef.equals("-"))
                    coef = "-1";
            } else
                coef = matcher.group();

            if (matcher.group().contains("^"))
                degree = matcher.group().substring(matcher.group().lastIndexOf('^') + 1);
            else {
                if (matcher.group().contains("x"))
                    degree = "1";
                else
                    degree = "0";
            }
            int coefValue, degreeValue;
            try {
                coefValue = Integer.parseInt(coef);
            } catch (NumberFormatException error) {
                JOptionPane.showMessageDialog(dashboard, "Invalid polynomial!");
                break;
            }
            try {
                degreeValue = Integer.parseInt(degree);
            } catch (NumberFormatException error) {
                JOptionPane.showMessageDialog(dashboard, "Invalid polynomial!");
                break;
            }
            p.addMonomial(new Monomial(coefValue, degreeValue));
        }
    }
}


