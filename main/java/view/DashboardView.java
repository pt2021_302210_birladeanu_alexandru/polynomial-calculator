package view;
import model.enums.OperationChoice;
import model.enums.PolynomialChoice;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class DashboardView extends JFrame {
    private final JTextField textField1;
    private final JTextField textField2;
    private final JLabel selectLabel;
    private final JComboBox comboBoxPolynomial;
    private final JComboBox comboBoxOperation;
    private final JButton runButton;
    private final JTextField textFieldResult;

    public DashboardView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 400, 400);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setBackground(new Color(0, 32, 63));
        contentPane.setLayout(null);

        JLabel firstLabel = new JLabel("FIRST POLYNOMIAL");
        firstLabel.setBounds(20, 10, 150, 20);
        firstLabel.setForeground(new Color(173, 239, 209));
        contentPane.add(firstLabel);

        JLabel lblNewLabel = new JLabel("SECOND POLYNOMIAL");
        lblNewLabel.setBounds(20, 60, 150, 20);
        lblNewLabel.setForeground(new Color(173, 239, 209));
        contentPane.add(lblNewLabel);

        textField1 = new JTextField();
        textField1.setBounds(20, 30, 350, 20);
        textField1.setBackground(new Color(173, 239, 209));
        textField1.setForeground(new Color(0, 32, 63));
        contentPane.add(textField1);
        textField1.setColumns(10);

        textField2 = new JTextField();
        textField2.setBounds(20, 80, 350, 20);
        textField2.setBackground(new Color(173, 239, 209));
        textField2.setForeground(new Color(0, 32, 63));
        contentPane.add(textField2);
        textField2.setColumns(10);

        JLabel lblNewLabel_1 = new JLabel("OPERATION");
        lblNewLabel_1.setBounds(20, 130, 85, 20);
        lblNewLabel_1.setForeground(new Color(173, 239, 209));
        contentPane.add(lblNewLabel_1);

        OperationChoice[] operationChoices = new OperationChoice[]{OperationChoice.ADD, OperationChoice.SUBTRACT, OperationChoice.MULTIPLY, OperationChoice.DIVIDE, OperationChoice.DERIVATIVE, OperationChoice.INTEGRAL};
        comboBoxOperation = new JComboBox<>(operationChoices);
        comboBoxOperation.setBounds(164, 130, 188, 21);
        comboBoxOperation.setBackground(new Color(173, 239, 209));
        comboBoxOperation.setForeground(new Color(0, 32, 63));
        contentPane.add(comboBoxOperation);

        selectLabel = new JLabel("SELECT POLYNOMIAL");
        selectLabel.setBounds(20, 160, 148, 21);
        selectLabel.setForeground(new Color(173, 239, 209));
        contentPane.add(selectLabel);
        selectLabel.setVisible(false);

        PolynomialChoice[] polynomialChoices = new PolynomialChoice[]{PolynomialChoice.FIRST, PolynomialChoice.SECOND};
        comboBoxPolynomial = new JComboBox<>(polynomialChoices);
        comboBoxPolynomial.setBounds(164, 160, 188, 21);
        comboBoxPolynomial.setBackground(new Color(173, 239, 209));
        comboBoxPolynomial.setForeground(new Color(0, 32, 63));
        contentPane.add(comboBoxPolynomial);
        comboBoxPolynomial.setVisible(false);

        runButton = new JButton("RUN");
        runButton.setBounds(141, 210, 85, 21);
        runButton.setBackground(new Color(0, 32, 63));
        runButton.setForeground(new Color(173, 239, 209));
        contentPane.add(runButton);

        JLabel lblNewLabel_2 = new JLabel("RESULT");
        lblNewLabel_2.setBounds(20, 280, 58, 20);
        lblNewLabel_2.setForeground(new Color(173, 239, 209));
        contentPane.add(lblNewLabel_2);

        textFieldResult = new JTextField();
        textFieldResult.setBounds(20, 300, 356, 20);
        textFieldResult.setBackground(new Color(173, 239, 209));
        textFieldResult.setForeground(new Color(0, 32, 63));
        contentPane.add(textFieldResult);
        textFieldResult.setColumns(10);
        textFieldResult.setEditable(false);

        this.setVisible(true);
    }

    public void comboBoxListener(ActionListener actionListener) {
        this.comboBoxOperation.addActionListener(actionListener);
    }

    public OperationChoice getOperationComboBox() {
        return (OperationChoice) this.comboBoxOperation.getSelectedItem();
    }

    public JLabel getSelectLabel() {
        return this.selectLabel;
    }

    public PolynomialChoice getPolynomialChoice() {
        return (PolynomialChoice) this.comboBoxPolynomial.getSelectedItem();
    }

    public JComboBox getPolynomialComboBox(){
        return this.comboBoxPolynomial;
    }

    public void runButtonListener(ActionListener actionListener) {
        this.runButton.addActionListener(actionListener);
    }

    public String getPolynomial1() {
        return this.textField1.getText();
    }

    public String getPolynomial2() {
        return this.textField2.getText();
    }

    public void setResult(String newResult) {
        this.textFieldResult.setText(newResult);
    }

    public JButton getRunButton(){
        return this.runButton;
    }
}
