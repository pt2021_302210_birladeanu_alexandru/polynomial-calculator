package model;

import java.util.*;

public class Polynomial {
    private final List<Monomial> monomialList = new ArrayList<>();

    public void addMonomial(Monomial newMonomial) {
        if (newMonomial.getCoef() != 0) {
            boolean found = false;
            for (Monomial m : monomialList) {
                if (newMonomial.getDegree() == m.getDegree()) {
                    found = true;
                    m.setCoef(m.getCoef() + newMonomial.getCoef());
                }
            }
            if (!found)
                monomialList.add(newMonomial);
            monomialList.sort(Collections.reverseOrder());
        }
    }

    public String toString() {
        if(monomialList.isEmpty())
            return "0";
        else {
            StringBuilder output = new StringBuilder();
            for (Monomial m : monomialList) {
                if (m.getCoef() > 0)
                    output.append('+');
                output.append(m.toString());
            }
            if (monomialList.get(0).getCoef() > 0)
                output = new StringBuilder(output.substring(1));
            return output.toString();
        }
    }

    public List<Monomial> getMonomialList() {
        return monomialList;
    }
}
