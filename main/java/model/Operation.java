package model;

public abstract class Operation {

    public static Polynomial addition(Polynomial p1, Polynomial p2) {
        Polynomial result = new Polynomial();
        int i1 = 0, i2 = 0;
        while (i1 < p1.getMonomialList().size() || i2 < p2.getMonomialList().size()) {
            if (i1 >= p1.getMonomialList().size()) {
                result.addMonomial(new Monomial(p2.getMonomialList().get(i2).getCoef(), p2.getMonomialList().get(i2).getDegree()));
                i2++;
            } else {
                if (i2 >= p2.getMonomialList().size()) {
                    result.addMonomial(new Monomial(p1.getMonomialList().get(i1).getCoef(), p1.getMonomialList().get(i1).getDegree()));
                    i1++;
                } else {
                    if (p1.getMonomialList().get(i1).getDegree() == p2.getMonomialList().get(i2).getDegree()) {
                        result.addMonomial(new Monomial(p1.getMonomialList().get(i1).getCoef() + p2.getMonomialList().get(i2).getCoef(), p1.getMonomialList().get(i1).getDegree()));
                        i1++;
                        i2++;
                    } else {
                        if (p1.getMonomialList().get(i1).getDegree() > p2.getMonomialList().get(i2).getDegree()) {
                            result.addMonomial(new Monomial(p1.getMonomialList().get(i1).getCoef(), p1.getMonomialList().get(i1).getDegree()));
                            i1++;
                        } else {
                            result.addMonomial(new Monomial(p2.getMonomialList().get(i2).getCoef(), p2.getMonomialList().get(i2).getDegree()));
                            i2++;
                        }
                    }
                }
            }
        }
        return result;
    }

    public static Polynomial subtraction(Polynomial p1, Polynomial p2) {
        Polynomial result = new Polynomial();
        int i1 = 0, i2 = 0;
        while (i1 < p1.getMonomialList().size() || i2 < p2.getMonomialList().size()) {
            if (i1 >= p1.getMonomialList().size()) {
                result.addMonomial(new Monomial(- p2.getMonomialList().get(i2).getCoef(), p2.getMonomialList().get(i2).getDegree()));
                i2++;
            } else {
                if (i2 >= p2.getMonomialList().size()) {
                    result.addMonomial(new Monomial(p1.getMonomialList().get(i1).getCoef(), p1.getMonomialList().get(i1).getDegree()));
                    i1++;
                } else {
                    if (p1.getMonomialList().get(i1).getDegree() == p2.getMonomialList().get(i2).getDegree()) {
                        result.addMonomial(new Monomial(p1.getMonomialList().get(i1).getCoef() - p2.getMonomialList().get(i2).getCoef(), p1.getMonomialList().get(i1).getDegree()));
                        i1++;
                        i2++;
                    } else {
                        if (p1.getMonomialList().get(i1).getDegree() > p2.getMonomialList().get(i2).getDegree()) {
                            result.addMonomial(new Monomial(p1.getMonomialList().get(i1).getCoef(), p1.getMonomialList().get(i1).getDegree()));
                            i1++;
                        } else {
                            result.addMonomial(new Monomial(- p2.getMonomialList().get(i2).getCoef(), p2.getMonomialList().get(i2).getDegree()));
                            i2++;
                        }
                    }
                }
            }
        }
        return result;
    }

    public static Polynomial derivative(Polynomial p) {
        Polynomial result = new Polynomial();
        for (Monomial m : p.getMonomialList()) {
            result.addMonomial(new Monomial(m.getCoef() * m.getDegree(), m.getDegree() - 1));
        }
        return result;
    }

    public static Polynomial integral(Polynomial p) {
        Polynomial result = new Polynomial();
        for (Monomial m : p.getMonomialList()) {
            result.addMonomial(new Monomial(m.getCoef() / (m.getDegree() + 1), m.getDegree() + 1));
        }
        return result;
    }

    public static Polynomial multiplication(Polynomial p1, Polynomial p2) {
        Polynomial result = new Polynomial();
        for (Monomial m1 : p1.getMonomialList()) {
            for (Monomial m2 : p2.getMonomialList()) {
                result.addMonomial(new Monomial(m1.getCoef() * m2.getCoef(), m1.getDegree() + m2.getDegree()));
            }
        }
        return result;
    }

    public static Polynomial division(Polynomial p1, Polynomial p2) {
        Polynomial result;
        if (p1.getMonomialList().get(0).getDegree() > p2.getMonomialList().get(0).getDegree())
            result = actualDivision(p1, p2);
        else
            result = actualDivision(p2, p1);
        return result;
    }

    private static Polynomial actualDivision(Polynomial higher, Polynomial lower) {
        Polynomial quotient = new Polynomial();

        while (higher.getMonomialList().get(0).getDegree() >= lower.getMonomialList().get(0).getDegree()) {
            Monomial newQuotientMonomial = new Monomial(higher.getMonomialList().get(0).getCoef() / lower.getMonomialList().get(0).getCoef(), higher.getMonomialList().get(0).getDegree() - lower.getMonomialList().get(0).getDegree());
            quotient.getMonomialList().add(newQuotientMonomial);

            Polynomial newQuotientMonomialOnly = new Polynomial();
            newQuotientMonomialOnly.addMonomial(newQuotientMonomial);
            higher = Operation.subtraction(higher, Operation.multiplication(newQuotientMonomialOnly, lower));
        }
        return quotient;
    }
}
