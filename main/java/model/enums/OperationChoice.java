package model.enums;

public enum OperationChoice {
    ADD,
    SUBTRACT,
    MULTIPLY,
    DIVIDE,
    DERIVATIVE,
    INTEGRAL,
}
