package model.enums;

public enum PolynomialChoice {
    FIRST,
    SECOND
}
