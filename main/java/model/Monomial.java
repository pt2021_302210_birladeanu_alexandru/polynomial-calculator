package model;

public class Monomial implements Comparable<Monomial> {
    private int coef;
    private final int degree;

    public Monomial(int coef, int degree) {
        this.coef = coef;
        this.degree = degree;
    }

    public String toString() {
        if (coef == 1 || coef == -1) {
            if (degree == 0)
                return coef + "";
            else {
                if (degree == 1) {
                    if (coef == 1)
                        return "x";
                    else
                        return "-x";
                } else if (coef == 1)
                    return "x^" + this.degree;
                else
                    return "-x^" + this.degree;
            }
        } else {
            if (degree == 0)
                return this.coef + "";
            else {
                if (degree == 1)
                    return this.coef + "x";
                else
                    return this.coef + "x^" + this.degree;
            }
        }
    }

    @Override
    public int compareTo(Monomial other) {
        Integer thisDegree = this.degree;
        Integer otherDegree = other.degree;
        return thisDegree.compareTo(otherDegree);
    }

    public int getCoef() {
        return coef;
    }

    public int getDegree() {
        return degree;
    }

    public void setCoef(int newCoef) {
        coef = newCoef;
    }
}
